serviceName = "EdNBAService";
integrationObj = KNYMobileFabric.getIntegrationService(serviceName);


define({ 

  //flex move for Teams
  animateTeams:function(){
    this.view.btnTeams.skin="sknBtnSelected";
    this.view.btnPlayers.skin="sknBtnNormal";
    this.view.flxTeams.animate(
      kony.ui.createAnimation({
        "100": {
          "left": "0%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
      }, {

      });
    this.view.flxPlayers.animate(
      kony.ui.createAnimation({
        "100": {
          "left": "100%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
      }, {

      });

  },

  //flex move for players
  animatePlayers:function(){
    this.view.btnTeams.skin="sknBtnNormal";
    this.view.btnPlayers.skin="sknBtnSelected";

    if(page==1){

      //reset array to prevent duplicates.

      pageData=[];
      
      this.view.btnAnterior.isVisible=false;

      this.view.imgCache.isVisible=false;

      this.getPlayers();

    }

    this.view.flxPlayers.animate(
      kony.ui.createAnimation({
        "100": {
          "left": "0%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
      }, {

      });
    this.view.flxTeams.animate(
      kony.ui.createAnimation({
        "100": {
          "left": "-100%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
      }, {

      });

  },

  //Next Nav Button
  increasePage:function(){

    this.pauseNavigation();
    kony.application.showLoadingScreen();

    page = page + 1;

    this.view.btnAnterior.isVisible=true;

    this.getPlayers();
  },

  //Back Nav Button
  decreasePage:function(){

    page = page - 1;

    var cache_index=page-1;

    this.getPlayersTemp(page,cache_index);

    if (page==1){this.view.btnAnterior.isVisible=false;}



  },

  //When form is navigated
  onNavigate:function(){

    this.pauseNavigation();
    kony.application.showLoadingScreen();

    this.view.btnTeams.onClick=this.animateTeams;
    this.view.btnPlayers.onClick=this.animatePlayers;
    this.view.btnAnterior.onClick=this.decreasePage;
    this.view.btnSiguiente.onClick=this.increasePage;

    operationName =  "Teams";
    data= {};
    headers= {"x-rapidapi-host": "free-nba.p.rapidapi.com","x-rapidapi-key": "a60a34a776msh513f1bf4cbad8a1p1c74d8jsn0477648ae013"};
    integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);


  },
  operationSuccess:function(res){


    var teamsData = res.data;
    var teamsInfo=[];
    var teamsDetail={};

    this.view.segTeams.widgetDataMap = { lblTeamTitle:'fullname', imgTeamLogo:'image' };

    for (var i = 0 ; i < teamsData.length; i++){

      teamsDetail={};

      teamsDetail.id=teamsData[i].id;
      teamsDetail.name=teamsData[i].name;
      teamsDetail.fullname=teamsData[i].full_name;
      teamsDetail.city=teamsData[i].city;
      teamsDetail.conference=teamsData[i].conference;
      teamsDetail.division=teamsData[i].division;
      teamsDetail.abbreviation=teamsData[i].abbreviation;
      teamsDetail.image=teamsData[i].abbreviation.toLowerCase()+'.png';
      teamsInfo.push(teamsDetail);

      this.view.segTeams.setData(teamsInfo);


    }


    kony.application.dismissLoadingScreen();
    this.resumeNavigation();

  },
  operationFailure:function(res){
    alert("Teams Service Error: ");
  },

  //Cache Data
  getPlayersTemp:function(page,index){

    //delete the last array to avoid duplicates.
    pageData.pop();

    var playersData = pageData[index];

    var temp={}; 
    var tempInfo=[];
    this.view.lblPage.text = page.toString() +' of '+total_pages.toString();

    this.view.segPlayers.widgetDataMap = { lblPlayerName:'fullname',imgPlayerTeam:'image'};


    for (var i = 0 ; i < playersData.length; i++){

      temp={};


      temp.fullname = playersData[i].fullname;
      temp.id=playersData[i].id;
      temp.image= playersData[i].image;
      temp.team=playersData[i].team;
      temp.position=playersData[i].position;
      temp.weight=playersData[i].weight;
      temp.height=playersData[i].height;



      tempInfo.push(temp);
      this.view.segPlayers.setData(tempInfo);

    }

    //display icon to show Cache being used
    this.view.imgCache.isVisible=true;

  },


  //Get Players Service
  getPlayers:function(){

    operationName =  "Players";
    data= {"page": page,"per_page": "10"};
    headers= {"x-rapidapi-host": "free-nba.p.rapidapi.com","x-rapidapi-key": "a60a34a776msh513f1bf4cbad8a1p1c74d8jsn0477648ae013"};
    integrationObj.invokeOperation(operationName, headers, data, this.opSuccess, this.opFailure);

  },
  opSuccess:function(res){

    var playersData = res.data;


    var playersInfo=[];
    var playersDetail={};

    this.view.lblPage.text = res.meta.current_page+' of '+res.meta.total_pages;
    total_pages=res.meta.total_pages;

    this.view.segPlayers.widgetDataMap = { lblPlayerName:'fullname',imgPlayerTeam:'image'};


    for (var i = 0 ; i < playersData.length; i++){
	

      playersDetail={};


      playersDetail.fullname = playersData[i].first_name + ' '+ playersData[i].last_name;
      playersDetail.id=playersData[i].id;
      playersDetail.image= playersData[i].team.abbreviation.toLowerCase()+'.png';
      playersDetail.team=playersData[i].team.full_name;
      playersDetail.position=playersData[i].position;
      playersDetail.weight=playersData[i].weight_pounds;
      playersDetail.height=playersData[i].height_feet;

      playersInfo.push(playersDetail);

      this.view.segPlayers.setData(playersInfo);


    }
    //assign data for cache from current service call
    pageData.push(playersInfo);
    this.view.imgCache.isVisible=false;

    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  },
  opFailure:function(res){
    alert("Players Service Error: "+ res.exception);
  }






});