define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onRowClick defined for segTeams **/
    AS_Segment_j46998881b2c4cf9b71d532e8c12bd56: function AS_Segment_j46998881b2c4cf9b71d532e8c12bd56(eventobject, sectionNumber, rowNumber) {
        var self = this;
        for (var i = 0; i < this.view.segTeams.selectedRowItems.length; i++) {
            var teamSelected = this.view.segTeams.selectedRowItems[i];
            var ntf = new kony.mvc.Navigation("frmTeamsDetails");
            ntf.navigate(teamSelected);
        }
    },
    /** onRowClick defined for segPlayers **/
    AS_Segment_fe6cab2fddf14479ac12f7a93252a632: function AS_Segment_fe6cab2fddf14479ac12f7a93252a632(eventobject, sectionNumber, rowNumber) {
        var self = this;
        for (var i = 0; i < this.view.segPlayers.selectedRowItems.length; i++) {
            var playerSelected = this.view.segPlayers.selectedRowItems[i];
            var ntf = new kony.mvc.Navigation("frmPlayersDetails");
            ntf.navigate(playerSelected);
        }
    }
});