serviceName = "EdNBAService";
integrationObj = KNYMobileFabric.getIntegrationService(serviceName);

define({ 

onNavigate:function(team){
	
  
this.setData();
 

  //close button
this.view.iconClose.skin= "sknCloseNormal";
this.view.btnClose.onClick=this.closeForm;

},
  
  setData:function(){
    
      //assign team data
  this.view.lblName.text = "Name: "+team.fullname;
  this.view.lblDiv.text = "Division: "+team.division;
  this.view.lblCity.text = "City: "+team.city;
  this.view.lblAb.text = "Abbreviation: "+team.abbreviation;
  this.view.imgTeam.src = team.abbreviation.toLowerCase()+'.png';
    
  },
  
  closeForm:function(){
    this.view.iconClose.skin = "sknClosePressed";
    var ntf = new kony.mvc.Navigation("frmMain");
    ntf.navigate();
    
  }



 });