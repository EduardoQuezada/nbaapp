define({ 

onNavigate:function(player){
  
  
  //assign player data
  this.view.lblTeam.text="Team: "+player.team;
  this.view.lblFullName.text="Name: "+player.fullname;

  this.view.imgPlayerTeam.src=player.image;
  
    //check if player Position is available.
  if (player.position==='' || player.position === null)
  {
    this.view.lblPos.text="Position: N/A";
  }
  else{
   this.view.lblPos.text="Position: "+player.position;
  }
  
  //check if player Height is available.
  if (typeof player.height=='undefined')
  {
    this.view.lblHeight.text="Height: N/A";
  }
  else{
    this.view.lblHeight.text="Height: "+player.height +" Feet";
  }
  
    //check if player Weight is available.
  if ( typeof player.weight=='undefined') 
  {
    this.view.lblWeight.text="Weight: N/A";
  }else
  {
    this.view.lblWeight.text="Weight: "+player.weight +" Pounds";
  }
  


  
  
  //Exit Button
  this.view.iconClose.skin= "sknCloseNormal";
this.view.btnClose.onClick=this.closeForm;
  
  
},
  
    closeForm:function(){
    this.view.iconClose.skin = "sknClosePressed";
      
     var ntf = new kony.mvc.Navigation("frmMain");
     ntf.navigate();
    
  }


 });